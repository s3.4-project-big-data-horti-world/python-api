# Routes

**Get /**
- desc:     route to test api
- returns:  `"Tomatoes for life"`
- *broken due to bug we don't know to resolve.

Post /images
- desc:     compresses images, saves to server, runs against ai model, saves results in db
- use key:  `image[]`
- returns:  
```json 
    {
        "0": {
        "img": "http://localhost:1337/images?path=12-06-2020/11:31:03-6543512324.jpg",
        "healthy": 0,
        "accuracy": 2
        },
        "1": {
            "img": "http://localhost:1337/images?path=12-06-2020/11:31:04-5691211061.jpg",
            "healthy": 0,
            "accuracy": 7
        },
        "2": {
            "img": "http://localhost:1337/images?path=12-06-2020/11:31:05-1996806774.jpg",
            "healthy": 1,
            "accuracy": 82
        }
    }
```

**GET /images/from/2020-06-04/until/2020-06-10**
- desc: returns information on images on certain dates. The link can be used to retrieve the images themselves.
- returns: 
```json
    {
        "0": {
            "id": 169,
            "name": "14:24:08-7596069563-c.jpg",
            "image": "http://localhost:1337/images?path=11-06-2020/14:24:08-7596069563-c.jpg",
            "healthy": 1,
            "accuracy": 51,
            "upload_DateTime": "2020-06-11 14:24:09"
        },
        "1": {
            "id": 170,
            "name": "14:24:10-2820398111-c.jpg",
            "image": "http://localhost:1337/images?path=11-06-2020/14:24:10-2820398111-c.jpg",
            "healthy": 1,
            "accuracy": 94,
            "upload_DateTime": "2020-06-11 14:24:12"
        },
    }
```

**GET /images?path=11-06-2020/15:31:57-1780013149.jpg**
- desc: gets image from server. Only allowed to get .jpg files.
- returns: image

**GET /stats**
- desc: get various usage statistics and model statistics.
- returns:
```json
    {
        "uploaded_images": 42,
        "processed_images": 116,
        "percentage_healthy_tomatoes": "98.3",
        "percentage_unhealthy_tomatoes": "1.7",
        "model_accuracy": "90"
    }
```

# How to run locally

**Run in docker:**
- clone this repo.
- copy .env-dist to .env and fill it.
- create a docker-compose.yml outside of the python-api directory. Fill it with:
```yml
    version: '3'

    services:
    api:
        build: python-api
        ports: 
        - 1337:1337
        environment:
        - HOST=0.0.0.0
        volumes: 
        - "./python-api:/app"
        - "./img:/img"
```
- create an img folder outside of the python-api directory.
- Your folder structure should look like this:
```
.
└── big-data-horti-world
    ├── docker-compose.yml
    ├── img
    ├── nuxt-frontend
    ├── python-api
    │   ├── api
    │   │   ├── api.py
    │   │   ├── model.py
    │   │   ├── settings.py
    │   ├── .env
    │   ├── .env-dist
    │   ├── .gitignore
    │   ├── dependencies.txt
    │   ├── Dockerfile
    │   ├── model
    │   │   └── model_v0.h5
    │   └── README.md
    └── python-scripts
```
- run `docker-compose up`