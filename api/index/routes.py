from api import app
from flask import Response
import jsonpickle

# Troll route
@app.route('/', methods=['GET'])
def home():
    return Response(response=jsonpickle.encode("Tomatoes for life"), status=200, mimetype="application/json")