from api import app, modelValidationLoss, modelAccuracy
from flask import Response
from .models import getUploadedImages, getProcessedImages, getPercentageHealthyAndUnhealthy
import jsonpickle

import db
import logging
import os

@app.route('/stats/', methods=['GET'])
def getStats():

    # Stats to get:
    # 1. Uploaded images
    # 2. Processed images
    # 3. Percentage healthy tomatoes
    # 4. Percentage unhealthy tomatoes
    # 5. Model accuracy

    uploaded_images = getUploadedImages()

    #2. Processed images
    processed_images = getProcessedImages()


    # 3. Percentage healthy and unhealthy
    percentage = getPercentageHealthyAndUnhealthy(processed_images)
    healthy_percentage = percentage[0]
    unhealthy_percentage = percentage[1]

    # Build response
    response = {
        "uploaded_images": uploaded_images,
        "processed_images": processed_images,
        "percentage_healthy_tomatoes": str(healthy_percentage),
        "percentage_unhealthy_tomatoes": str(unhealthy_percentage),
        "model_accuracy": modelAccuracy,
        "model_validation_loss": modelValidationLoss
    }

    # Return response
    response_pickled = jsonpickle.encode(response)
    return Response(response=response_pickled, status=200, mimetype="application/json")