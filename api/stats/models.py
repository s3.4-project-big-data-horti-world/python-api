import db
import logging
import os

def getUploadedImages():
    #1. Uploaded images
    uploaded_images = 0
    for root, dirs, files in os.walk(os.getenv("IMG_DIR")):
        uploaded_images = uploaded_images + len(files)

    return uploaded_images


def getProcessedImages():
    query = """
        SELECT COUNT(path) 
        FROM `images` 
        WHERE `path` LIKE %(img_folder)s
        """
    recordTuple = {
        'img_folder': os.getenv("IMG_DIR") + '%'
    }
    results = db.selectFromDB(query, recordTuple)
    return results[0][0]

def getPercentageHealthyAndUnhealthy(processed_images):
    query = """
        SELECT 
            CAST(
                (SELECT COUNT(healthy) AS 'healthy' 
                FROM `images` 
                WHERE healthy LIKE 1 AND path LIKE %(img_folder)s) 
                * 100 / %(total_images)s AS decimal(10,1)) AS healthypercentage,
            CAST(
                (SELECT COUNT(healthy) AS 'unhealthy' 
                FROM `images` WHERE healthy LIKE 0 AND path LIKE %(img_folder)s) 
                * 100 / %(total_images)s AS decimal(10,1)) AS unhealthypercentage
        FROM `images`
        GROUP BY healthy;
        """
    recordTuple = {
        'img_folder': os.getenv("IMG_DIR") + '%',
        'total_images': processed_images
    }
    results = db.selectFromDB(query, recordTuple)
    healthy_percentage = results[0][0]
    unhealthy_percentage = results[0][1]

    return (healthy_percentage, unhealthy_percentage)
