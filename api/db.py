import logging
import api


# This file contains REUSABLE database actions (like select and insert)

def insertIntoDB(query, recordTuple):
    try:
        conn = api.mysql.connect()
        cursor = conn.cursor()

        logging.info("Query to execute: " + cursor.mogrify(query, recordTuple))

        insertedRows = cursor.executemany(query, recordTuple)

        conn.commit()
        
        cursor.close()
        conn.close()

        logging.info("Query successfully executed!")
    except Exception as e:
        logging.error(str(e))


def selectFromDB(query, recordTuple):
    try:
        conn = api.mysql.connect()
        cursor = conn.cursor()

        logging.info("Query to execute: " + cursor.mogrify(query, recordTuple))

        cursor.execute(query, recordTuple)
        result = list(cursor.fetchall())
        
        cursor.close()
        conn.close()

        logging.info("Query successfully executed!")

        return result
    except Exception as e:
        logging.error(str(e))
