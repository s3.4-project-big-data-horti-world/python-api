from pathlib import Path
from dotenv import load_dotenv
import logging
from flask import Flask
from flaskext.mysql import MySQL
import os
import tensorflow as tf
from keras.models import load_model

def init():
    global app 
    global mysql
    global ALLOWED_EXTENSIONS
    global modelAccuracy
    global modelValidationLoss
    global h5model
    
    app = Flask(__name__) 
    mysql = MySQL()
    ALLOWED_EXTENSIONS = {'jpg'}

    # SET MODEL STATS AND MODEL TO USE
    modelAccuracy = 87
    modelValidationLoss = 37
    model = 'model/model_v0.h5'

     # load the model we saved
    h5model = tf.keras.models.load_model(model)
    h5model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

    import index.routes
    import stats.routes
    import image.routes

    app.config["DEBUG"] = os.getenv("DEBUG")
    app.config['MYSQL_DATABASE_USER'] = os.getenv('MYSQL_DATABASE_USER')
    app.config['MYSQL_DATABASE_PASSWORD'] = os.getenv('MYSQL_DATABASE_PASSWORD')
    app.config['MYSQL_DATABASE_DB'] = os.getenv('MYSQL_DATABASE_DB')
    app.config['MYSQL_DATABASE_HOST'] = os.getenv('MYSQL_DATABASE_HOST')
    app.config['MAX_CONTENT_LENGTH'] = 16 * 3840 * 3840

    mysql.init_app(app)


# Path to '.env'
env_path = Path('.') / '.env'
print("envpath: " + str(env_path))
load_dotenv(dotenv_path=env_path)

# set logging level
logging.root.setLevel(logging.INFO)

# Start app
init()

app.run(host=os.getenv("HOST"), port=os.getenv("PORT"), debug=os.getenv("DEBUG"))