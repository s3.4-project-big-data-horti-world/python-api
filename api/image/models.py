import api
import db
import numpy as np
import cv2
import logging
import datetime
import os
from flask import Response
import re
from random import randint
from PIL import Image
from io import BytesIO
from keras.preprocessing.image import load_img
from keras.preprocessing import image

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in api.ALLOWED_EXTENSIONS
   

def processImage(uploadedImg):
    # 1. READ IMAGE
    img = Image.open(BytesIO(uploadedImg))
    # img.mode = "RGBA"

    #2. COMPRESS IMAGE 
    imgPath = compressAndSaveImage(img)

    # 3. TEST IMAGE AGAINST MODEL
    healthy = True 
    model_result = test_image_against_model(imgPath)

    if (model_result < 0.5):
        healthy = False
    accuracy = model_result * 100

    # build a response dict to send back to client
    result = ( imgPath, int(healthy), int(accuracy) )
    
    return result


def saveImagesInDB(results):   
    print("save Images In db", results)
    query = """
        INSERT INTO `images` (`path`, `healthy`, `accuracy`) 
        VALUES (%s, %s, %s);
    """
    db.insertIntoDB(query, results)


def searchImagesInDB(from_date, until_date):
    # 2. Create query
    # Note: path is based off environment. It gets local images when not running in prod.
    query = """
        SELECT `id`, `datetime`,`path`,`healthy`,`accuracy` 
        FROM images 
        WHERE datetime BETWEEN  %(from_date)s AND %(until_date)s AND path LIKE %(img_folder)s
    """
    recordTuple = {
        'from_date': from_date,
        'until_date': until_date,
        'img_folder': os.getenv("IMG_DIR") + '%'
    }
    return db.selectFromDB(query, recordTuple)


def test_image_against_model(imgPath):
    logging.info("Predicting image...")
    # predicting images 
    img = load_img(imgPath, target_size=(300, 200))
    img = image.img_to_array(img)  
    img = img.reshape(1, 300, 200, 3)
 
    # predict using model
    result = api.h5model.predict(img)
    logging.info("result: {}".format(result[0]))
    return result[0]



def compressAndSaveImage(img):
    logging.info("Compressing Image...")

    # 1. Get path where image should be saved

    # Create directory if not exists for date of today
    today = datetime.date.today().strftime("%d-%m-%Y")
    directory = os.getenv("IMG_DIR") + today
    if not os.path.exists(directory): 
        os.makedirs(directory)
        logging.info("Directory for " + today + " created")
    else:   
        logging.info("Directory for " + today + " already exists")
 
    # Create random string of numbers in order for a different image name for each image
    randomIntArray = [randint(0, 9) for p in range(0, 10)]
    randomString = ""
    for int in randomIntArray:
        randomString += str(int)
    
    # Generate img path
    time = datetime.datetime.now().strftime("%H:%M:%S")
    imgPath = os.getenv("IMG_DIR") + today + "/" + time + "-" + randomString +".jpg"

    # 2. Compress image
       
    # Calculate new width and height, while keeping the aspect ratio
    width, height = img.size 
    newHeight = 500
    newWidth = round((newHeight / width) * height)
  
    try:
        logging.info("Saving compressed Image...") 

        imgSmall = img.resize((newHeight,newWidth),Image.ANTIALIAS) 
        imgSmall.save(imgPath, optimize=True, quality=95)

        logging.info("Image size: " + str(os.path.getsize(imgPath)) )

    except Exception as e:
        logging.error("Error saving compressed image: " + str(e))
        raise e

    return imgPath

 