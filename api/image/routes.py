from api import app
from flask import request, Response
import jsonpickle
from .models import allowed_file, processImage, saveImagesInDB, searchImagesInDB

import db
import datetime
import logging
import os
import re
from flask import send_from_directory

# Send images to ai and save to database
@app.route('/images', methods=['POST'])
def test():

    # Get  a list of all images
    uploaded_images = request.files.getlist("image[]")

    result = []
    response = dict()
    i = 0

    # Loop through the list of images to process
    for img in uploaded_images:

        imgResponse = dict() 

        # Do not process image if it is not an allowed extension
        if img and allowed_file(img.filename):
            imgResult = processImage(img.read())
            imgUrl = "http://" + os.getenv("DOMAIN") + "/images?path=" + imgResult[0].replace(os.getenv("IMG_DIR"), '')
            imgResponse = {"img": imgUrl, "healthy": imgResult[1], "accuracy": imgResult[2]}
        else:
            imgResponse = {'img': img.filename, 'error': "Only .jpg images allowed"}
        
        # Build query params 
        result.append(imgResult)

        # Build response
        response[i] = imgResponse
        i = i + 1
 

    # Save all images in db
    saveImagesInDB(result)  
        

    # Return response
    response_pickled = jsonpickle.encode(response)
    return Response(response=response_pickled, status=200, mimetype="application/json")



# Get image url and results from ai between certain dates
@app.route('/images/from/<string:from_date>/until/<string:until_date>', methods=['GET'])
def getImageUrlByDate(from_date, until_date):

    # 1. Validate & reformat date
    try:
        from_date = datetime.datetime.strptime(from_date, '%Y-%m-%d')
        until_date = datetime.datetime.strptime(until_date, '%Y-%m-%d')
        until_date = until_date.replace(minute=59, hour=23, second=59)
    except ValueError:
        return Response( 
            response=str(ValueError("Incorrect data format, should be YYYY-MM-DD")), 
            status=500
            )

    logging.info("Dates validated and reformatted to from_date: `" + str(from_date) + "` and until_date: `" + str(until_date) + "`")

    results = searchImagesInDB(from_date, until_date)

    # 3. Build response
    response = dict()
    i = 0
    for result in results:
        reImgName = re.search("([^\/]+$)", result[2] )
        name = reImgName.group(0)
        image = "http://" + os.getenv("DOMAIN") + "/images?path=" + result[2].replace(os.getenv("IMG_DIR"), '')
        response[i] = {
            "id": result[0], 
            "name": name,
            "image": image,
            "healthy": result[3],
            "accuracy": result[4],
            "upload_DateTime": str(result[1]),
            }
        i = i + 1

    response_pickled = jsonpickle.encode(response)
    return Response(response=response_pickled, status=200, mimetype="application/json")

# Get images
@app.route('/images', methods=['GET'])
def getImage():
    filename = request.args.get('path')
    directory = os.getenv("IMG_DIR") 
    
    logging.info("Getting image from current directory " + directory + " and path " + filename)

    # Only allow .jpg files
    if allowed_file(filename) == False:
        return Response(response="Only .jpg files allowed", status=403)

    # No try catch needed since this function returns a 404 already 
    # if the image is not found
    return send_from_directory(directory=directory, filename=filename)


