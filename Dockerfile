FROM python:3.7

ENV TZ=Europe/Amsterdam
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#ARG UID
#ARG GID  

#RUN addgroup --gid ${GID} user
#RUN adduser --disabled-password --gecos '' --uid ${UID} --gid ${GID} user

# RUN chown -R ${UID}:${GID} /app
# RUN chown -R ${UID}:${GID} /img

#USER ${UID}

RUN mkdir app img

COPY . /app

WORKDIR /app

RUN pip3 install -r dependencies.txt

EXPOSE 80

CMD python3 api/api.py
